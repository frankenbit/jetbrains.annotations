using System;

namespace JetBrains.Annotations
{
    [AttributeUsage( AttributeTargets.Assembly, AllowMultiple = true )]
    public sealed class AspMvcPartialViewLocationFormatAttribute : Attribute
    {
        // ReSharper disable once UnusedParameter.Local
        public AspMvcPartialViewLocationFormatAttribute( string format )
        {
        }
    }
}