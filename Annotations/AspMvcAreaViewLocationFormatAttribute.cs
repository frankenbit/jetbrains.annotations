using System;

namespace JetBrains.Annotations
{
    [AttributeUsage( AttributeTargets.Assembly, AllowMultiple = true )]
    public sealed class AspMvcAreaViewLocationFormatAttribute : Attribute
    {
        // ReSharper disable once UnusedParameter.Local
        public AspMvcAreaViewLocationFormatAttribute( string format )
        {
        }
    }
}