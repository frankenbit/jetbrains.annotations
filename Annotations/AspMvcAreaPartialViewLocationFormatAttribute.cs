using System;

namespace JetBrains.Annotations
{
    [AttributeUsage( AttributeTargets.Assembly, AllowMultiple = true )]
    public sealed class AspMvcAreaPartialViewLocationFormatAttribute : Attribute
    {
        // ReSharper disable once UnusedParameter.Local
        public AspMvcAreaPartialViewLocationFormatAttribute( string format )
        {
        }
    }
}