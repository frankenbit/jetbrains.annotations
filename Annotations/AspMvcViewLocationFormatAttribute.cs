using System;

namespace JetBrains.Annotations
{
    [AttributeUsage( AttributeTargets.Assembly, AllowMultiple = true )]
    public sealed class AspMvcViewLocationFormatAttribute : Attribute
    {
        // ReSharper disable once UnusedParameter.Local
        public AspMvcViewLocationFormatAttribute( string format )
        {
        }
    }
}