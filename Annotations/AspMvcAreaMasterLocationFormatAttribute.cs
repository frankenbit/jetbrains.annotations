using System;

namespace JetBrains.Annotations
{
    [AttributeUsage( AttributeTargets.Assembly, AllowMultiple = true )]
    public sealed class AspMvcAreaMasterLocationFormatAttribute : Attribute
    {
        // ReSharper disable once UnusedParameter.Local
        public AspMvcAreaMasterLocationFormatAttribute( string format )
        {
        }
    }
}