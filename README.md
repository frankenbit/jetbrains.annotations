# README #

JetBrains ReSharper Annotation Classes shared between various Unity3D projects.

### What is this repository for? ###

* This repository is usually added as a GIT submodule at `Assets/JetBrains` to provide ReSharper annotations to Unity3D projects using GIT mechanisms without the need to add and maintain them manually to each project.

### How do I get set up? ###

* Create a new Unity3D project or select an existing one.
* Ensure that the selected project is tracked in a GIT repository.
* Add a new submodule from the `git@bitbucket.org:frankenbit/jetbrains.annotations.git` repository at the `Assets/JetBrains` path.

### Contribution guidelines ###

* All annotation classes are provided by JetBrains / ReSharper, so there should be not much need to contribute to this repository.
* Use the project issue tracker to request removal of any build warnings, modifications to the project setup or whatever else.

### Who do I talk to? ###

* Repo owner or admin